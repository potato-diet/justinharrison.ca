---
title: Migration to Middleman
date: 2016-02-15
tags: Programming
---

I've finally thrown out my jerry rigged static-site buider, and just went with
Middleman. Middleman also supports Markdown-based blogging, so I'm getting rid
of my Ghost blog.
